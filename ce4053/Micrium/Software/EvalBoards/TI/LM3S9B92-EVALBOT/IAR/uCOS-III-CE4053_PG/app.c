/*
*********************************************************************************************************
*                                              EXAMPLE CODE
*
*                          (c) Copyright 2009-2010; Micrium, Inc.; Weston, FL
*
*               All rights reserved.  Protected by international copyright laws.
*               Knowledge of the source code may NOT be used to develop a similar product.
*               Please help us continue to provide the Embedded community with the finest
*               software available.  Your honesty is greatly appreciated.
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*
*                                            EXAMPLE CODE
*
* Filename      : app.c
* Version       : V1.00
* Programmer(s) : FUZZI
*
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*                                             INCLUDE FILES
*********************************************************************************************************
*/

#include <includes.h>
#include <stdio.h>
#include "driverlib/timer.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include <os.h>
#include <cmath>
#include <cstdlib>

/*
*********************************************************************************************************
*                                             Fibonacci function and node declaration,local var
*********************************************************************************************************
*/
/*
 * Node Declaration
 */
#define MAX 10000

struct node
{
    CPU_INT32U  n; //n is the period value
    CPU_INT32U  degree;
    CPU_INT32U  cur_ctr;
    CPU_INT32U  nxt_match_ctr; //n is the next tick counter match value
    CPU_INT32U  remain_ctr; // use for fibo sort
    CPU_INT32U  exe; //indicate if executed in the current counter or not
    struct node* parent;
    struct node* child;
    struct node* left;
    struct node* right;
};

struct rdy_q_node {
    struct rdy_q_node * parent;
    struct rdy_q_node * left;
    struct rdy_q_node * right;
    CPU_INT32U value;
    CPU_INT32U index;
    CPU_INT32U n; //record period
};

//Time measurement
CPU_INT32U   StartTime = 0;  
CPU_INT32U   StartTime2 = 0; 
CPU_INT32U   OverheadValue = 0;  

//For AVL physical node creation
static struct rdy_q_node * root;
static struct rdy_q_node * nil;
static CPU_INT32U h[MAX]; // keep values for the heights of the sub trees
static CPU_INT32U tree_size;

struct rdy_q_node avl_null_node;
struct rdy_q_node t1_rdy;
struct rdy_q_node t2_rdy; 
struct rdy_q_node t3_rdy; 
struct rdy_q_node t4_rdy;
struct rdy_q_node t5_rdy; 


struct rdy_q_node* init_tree();
void left_rotate(struct rdy_q_node * x);
void right_rotate(struct rdy_q_node * x);
CPU_INT32U height(struct rdy_q_node * x);
void balance(struct rdy_q_node * x);
void add_to_rdy_q_node(struct rdy_q_node* tx_rdy,CPU_INT32U a, CPU_INT32U n);
void print(struct rdy_q_node * el, CPU_INT32U i);
struct rdy_q_node* search(CPU_INT32U x);
void transplant(struct rdy_q_node * node1,struct  rdy_q_node * node2);
struct rdy_q_node* minimum(struct rdy_q_node * node);
void delete_balance(struct rdy_q_node * x);
void delete_from_rdy_q_node(CPU_INT32U val);


struct node* Insert(struct node* H, struct node* x);
struct node* Extract_Min(struct node* x);
void Fibo_Sort(struct node* x);
//int Consolidate(struct node* H1);

int      nH = 0;
struct node* H;  //a node pointer that always point to the minimum
struct node* prev_exe_t_ptr;  //a node pointer that point to the previous executed task node
struct node  prev_exe_t;  //a copy of previous executed task node
struct node t1_leftLed;
struct node t2_rightLed; 
struct node t3_BothLed; 
struct node t4_moveForward;
struct node t5_moveBackward; 

CPU_INT16U   leftLEDBlink_executed = 0; 
CPU_INT16U   rightLEDBlink_executed = 0;   
CPU_INT16U   BothLEDBlink_executed = 0; 
CPU_INT16U   moveForward_executed = 0;   
CPU_INT16U   moveBackward_executed = 0;  
CPU_INT32U   prev_degree = 0;   //to decide weather to repeat and resort from the left most position again
CPU_INT32U   cur_degree = 0;   //to decide weather to repeat and resort from the left most position again
/*
*********************************************************************************************************
*                                             LOCAL DEFINES
*********************************************************************************************************
*/

#define ONESECONDTICK             7000000

//#define TASK1PERIOD                   10
//#define TASK2PERIOD                   20
#define TASK1PERIOD                   5
#define TASK2PERIOD                   7

#define WORKLOAD1                     1
#define WORKLOAD2                     4


#define TIMERDIV                      (BSP_CPUClkFreq() / (CPU_INT32U)OSCfg_TickRate_Hz)


/*
*********************************************************************************************************
*                                            LOCAL VARIABLES
*********************************************************************************************************
*/


static  OS_TCB       AppTaskStartTCB;
static  CPU_STK      AppTaskStartStk[APP_TASK_START_STK_SIZE];

static  CPU_STK      OS_RecTaskStk[OS_RecTask_STK_SIZE];

static  OS_TCB       leftLEDBlinkTCB;
static  CPU_STK      leftLEDBlinkStk[leftLEDBlink_STK_SIZE];

static  OS_TCB       rightLEDBlinkTCB;
static  CPU_STK      rightLEDBlinkStk[rightLEDBlink_STK_SIZE];

static  OS_TCB       BothLEDBlinkTCB;
static  CPU_STK      BothLEDBlinkStk[BothLEDBlink_STK_SIZE];

static  OS_TCB       moveForwardTCB;
static  CPU_STK      moveForwardStk[moveForward_STK_SIZE];

static  OS_TCB       moveBackwardTCB;
static  CPU_STK      moveBackwardStk[moveBackward_STK_SIZE];


CPU_INT32U      iCnt = 0;
CPU_INT08U      Left_tgt;
CPU_INT08U      Right_tgt;
CPU_INT32U      iToken  = 0;
CPU_INT32U      iCounter= 1;
CPU_INT32U      iMove   =4;
CPU_INT32U      measure=0;


/* Design */
CPU_INT32U      leftLedDone=0;
CPU_INT32U      rightLedDone=0;
CPU_INT32U      BothLedDone=0;

/*
*********************************************************************************************************
*                                         FUNCTION PROTOTYPES
*********************************************************************************************************
*/

static  void        AppRobotMotorDriveSensorEnable    ();
        void        IntWheelSensor                    ();
        void        RoboTurn                          (tSide dir, CPU_INT16U seg, CPU_INT16U speed);

static  void        AppTaskStart                 (void  *p_arg);

static  void        OS_RecTaskCreate             (void  *p_arg);
static  void        leftLEDBlink                 (void  *p_arg);
static  void        rightLEDBlink                (void  *p_arg);
static  void        BothLEDBlink                 (void  *p_arg);
static  void        moveForward                  (void  *p_arg);
static  void        moveBackward                 (void  *p_arg);
/*
*********************************************************************************************************
*                                                main()
*
* Description : This is the standard entry point for C code.  It is assumed that your code will call
*               main() once you have performed all necessary initialization.
*
* Arguments   : none
*
* Returns     : none
*********************************************************************************************************
*/

int  main (void)
{
    OS_ERR  err;

    BSP_IntDisAll();                                            /* Disable all interrupts.                              */
    OSInit(&err);                                               /* Init uC/OS-III.                                      */

    OSTaskCreate((OS_TCB     *)&AppTaskStartTCB,           /* Create the start task                                */
                 (CPU_CHAR   *)"App Task Start",
                 (OS_TASK_PTR ) AppTaskStart,
                 (void       *) 0,
                 (OS_PRIO     ) APP_TASK_START_PRIO,
                 (CPU_STK    *)&AppTaskStartStk[0],
                 (CPU_STK_SIZE) APP_TASK_START_STK_SIZE / 10u,
                 (CPU_STK_SIZE) APP_TASK_START_STK_SIZE,
                 (OS_MSG_QTY  ) 0u,
                 (OS_TICK     ) 0u,
                 (void       *) (CPU_INT32U) 0, 
                 (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 (OS_ERR     *)&err);

    OSStart(&err);                                              /* Start multitasking (i.e. give control to uC/OS-III). */
}


/* Design */
//function to keep counting
//void APP_CTR_FUNC ()
//{
  
//  APP_CTR++;
  
//  return;
//}

/*
*********************************************************************************************************
*                                          STARTUP TASK
*
* Description : This is an example of a startup task.  As mentioned in the book's text, you MUST
*               initialize the ticker only once multitasking has started.
*
* Arguments   : p_arg   is the argument passed to 'AppTaskStart()' by 'OSTaskCreate()'.
*
* Returns     : none
*
* Notes       : 1) The first line of code is used to prevent a compiler warning because 'p_arg' is not
*                  used.  The compiler should not generate any code for this statement.
*********************************************************************************************************
*/

static  void  AppTaskStart (void  *p_arg)
{
    CPU_INT32U  clk_freq;
    CPU_INT32U  cnts;
    OS_ERR      err;
    (void)&p_arg;
    BSP_Init();                                                 /* Initialize BSP functions                             */
    CPU_Init();                                                 /* Initialize the uC/CPU services                       */
    clk_freq = BSP_CPUClkFreq();                                /* Determine SysTick reference freq.                    */
    cnts     = clk_freq / (CPU_INT32U)OSCfg_TickRate_Hz;        /* Determine nbr SysTick increments                     */
    OS_CPU_SysTickInit(cnts);                                   /* Init uC/OS periodic time src (SysTick).              */
    CPU_TS_TmrFreqSet(clk_freq);
    
    /* Enable Wheel ISR Interrupt */
    AppRobotMotorDriveSensorEnable();
    
    
    /* Initialise the OS_RecTaskTCB task */
    
    //Create a recursive task , using OSTaskCreate, using the same stack size as TASK 1 size
    OSTaskCreate((OS_TCB     *)&OS_RecTaskTCB, (CPU_CHAR   *)"OS_RecTaskCreate", (OS_TASK_PTR ) OS_RecTaskCreate, (void       *) 0, (OS_PRIO     ) OS_RecTask_PRIO, (CPU_STK    *)&OS_RecTaskStk[0], (CPU_STK_SIZE) OS_RecTask_STK_SIZE / 10u, (CPU_STK_SIZE) OS_RecTask_STK_SIZE, (OS_MSG_QTY  ) 0u, (OS_TICK     ) 0u, (void       *) (CPU_INT32U) 2, (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), (OS_ERR     *)&err);
    
     while (DEF_ON) {
             
            }
    
}

static void OS_RecTaskCreate (void *p_arg)
{ 
   OS_ERR      err;
  (void)&p_arg;
  CPU_INT32U      APP_1SEC_CTR = 0;
  CPU_TS  ts;
  CPU_INT16U   leftLEDBlink_Exe_PRIO;                             
  CPU_INT16U   rightLEDBlink_Exe_PRIO;                            
  CPU_INT16U   BothLEDBlink_Exe_PRIO;  
  CPU_INT16U   moveForward_Exe_PRIO; 
  CPU_INT16U   moveBackward_Exe_PRIO;
  CPU_INT32U t1_deadline;
  CPU_INT32U t2_deadline;
  CPU_INT32U t3_deadline; 
  CPU_INT32U t4_deadline;
  CPU_INT32U t5_deadline;
  CPU_INT32U min;
  CPU_INT32U prev_prior;
  CPU_INT32U init_tree_v;
  CPU_INT32U avl_exe_right = 0;
  
  //For AVL
  struct rdy_q_node root_org; //always record the AVL root node physically befor OSCREATE TASK 
  struct rdy_q_node *min_rdy_q_node;
  min_rdy_q_node = NULL;  
  //End of AVL variable
  
  //Init H to a NULL value first
  H = NULL;
  //Create tast instances and insert,at initial value
  t1_leftLed.n = 5u;
  t1_leftLed.cur_ctr = 0u; 
  t1_leftLed.nxt_match_ctr = t1_leftLed.n + 0u; // n is period
  t1_leftLed.remain_ctr = t1_leftLed.nxt_match_ctr - t1_leftLed.cur_ctr; // cal remain count
  H = Insert(H,&t1_leftLed);
  
  t2_rightLed.n = 20u;
  t2_rightLed.cur_ctr = 0u; 
  t2_rightLed.nxt_match_ctr = t2_rightLed.n + 0u; // n is period
  t2_rightLed.remain_ctr = t2_rightLed.nxt_match_ctr - t2_rightLed.cur_ctr; // cal remain count
  H = Insert(H,&t2_rightLed);
  
  t3_BothLed.n = 15u;
  t3_BothLed.cur_ctr = 0u; 
  t3_BothLed.nxt_match_ctr = t3_BothLed.n + 0u; // n is period
  t3_BothLed.remain_ctr = t3_BothLed.nxt_match_ctr - t3_BothLed.cur_ctr; // cal remain count
  H = Insert(H,&t3_BothLed);

  
  t4_moveForward.n = 35u;
  t4_moveForward.cur_ctr = 0u; 
  t4_moveForward.nxt_match_ctr = t4_moveForward.n  + 0u; // n is period
  t4_moveForward.remain_ctr = t4_moveForward.nxt_match_ctr - t4_moveForward.cur_ctr; // cal remain count
  H = Insert(H,&t4_moveForward);

  
  t5_moveBackward.n = 60u;
  t5_moveBackward.cur_ctr = 0u; 
  t5_moveBackward.nxt_match_ctr = t5_moveBackward.n  + 0u; // n is period
  t5_moveBackward.remain_ctr = t5_moveBackward.nxt_match_ctr - t5_moveBackward.cur_ctr; // cal remain count
  H = Insert(H,&t5_moveBackward);
  
  
  //Start all the task at T=0
  OSTaskCreate((OS_TCB     *)&leftLEDBlinkTCB, (CPU_CHAR   *)"leftLEDBlink", (OS_TASK_PTR ) leftLEDBlink, (void       *) 0, (OS_PRIO     ) leftLEDBlink_PRIO, (CPU_STK    *)&leftLEDBlinkStk[0], (CPU_STK_SIZE) leftLEDBlink_STK_SIZE / 10u, (CPU_STK_SIZE) leftLEDBlink_STK_SIZE, (OS_MSG_QTY  ) 0u, (OS_TICK     ) 0u, (void       *) (CPU_INT32U) 2, (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), (OS_ERR     *)&err);  
  //while(leftLedDone != 1){};  //keep looping until the task is done
  //leftLedDone = 0;
 
  OSTaskCreate((OS_TCB     *)&BothLEDBlinkTCB, (CPU_CHAR   *)"BothLEDBlink", (OS_TASK_PTR ) BothLEDBlink, (void       *) 0, (OS_PRIO     ) BothLEDBlink_PRIO, (CPU_STK    *)&BothLEDBlinkStk[0], (CPU_STK_SIZE) BothLEDBlink_STK_SIZE / 10u, (CPU_STK_SIZE) BothLEDBlink_STK_SIZE, (OS_MSG_QTY  ) 0u, (OS_TICK     ) 0u, (void       *) (CPU_INT32U) 2, (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), (OS_ERR     *)&err);
  //while(BothLedDone != 1){};  //keep looping until the task is done
  //BothLedDone = 0;
 
  
  OSTaskCreate((OS_TCB     *)&rightLEDBlinkTCB, (CPU_CHAR   *)"rightLEDBlink", (OS_TASK_PTR ) rightLEDBlink, (void       *) 0, (OS_PRIO     ) rightLEDBlink_PRIO, (CPU_STK    *)&rightLEDBlinkStk[0], (CPU_STK_SIZE) rightLEDBlink_STK_SIZE / 10u, (CPU_STK_SIZE) rightLEDBlink_STK_SIZE, (OS_MSG_QTY  ) 0u, (OS_TICK     ) 0u, (void       *) (CPU_INT32U) 2, (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), (OS_ERR     *)&err);
  //while(rightLedDone != 1){};  //keep looping until the task is done
  //rightLedDone = 0;
 

  iMove = 2;
  OSTaskCreate((OS_TCB     *)&moveForwardTCB, (CPU_CHAR   *)"moveForward", (OS_TASK_PTR ) moveForward, (void       *) 0, (OS_PRIO     ) moveForward_PRIO, (CPU_STK    *)&moveForwardStk[0], (CPU_STK_SIZE) moveForward_STK_SIZE / 10u, (CPU_STK_SIZE) moveForward_STK_SIZE, (OS_MSG_QTY  ) 0u, (OS_TICK     ) 0u, (void       *) (CPU_INT32U) 2, (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), (OS_ERR     *)&err);


  iMove = 2;
  OSTaskCreate((OS_TCB     *)&moveBackwardTCB, (CPU_CHAR   *)"moveBackward", (OS_TASK_PTR ) moveBackward, (void       *) 0, (OS_PRIO     ) moveBackward_PRIO, (CPU_STK    *)&moveBackwardStk[0], (CPU_STK_SIZE) moveBackward_STK_SIZE / 10u, (CPU_STK_SIZE) moveBackward_STK_SIZE, (OS_MSG_QTY  ) 0u, (OS_TICK     ) 0u, (void       *) (CPU_INT32U) 2, (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), (OS_ERR     *)&err);

    //Start all the tast after time T=0, which is T>0
    while(1)
    {
      
        (void)OSTaskSemPend(0,
          OS_OPT_PEND_BLOCKING,
          &ts,
          &err);
         
         //StartTime = OS_TS_GET ();

         //StartTime2 = OS_TS_GET (); 

          //Init the AVL scheduler tree first if the init_tree is 0;
         if (init_tree_v == 0) {
           nil = init_tree();
           root = init_tree();
           init_tree_v = 1;
         }
         
         
          APP_1SEC_CTR = APP_1SEC_CTR + 1;
      
            
          //Implement fibonacci heap search for the match tick counter task and create and execute and delete the task of the match counter
          //always Search for the minimum period in the existing data structure fibo heap
          //If the APP_1SEC_CTR is match, it will launch, if not increment  the counter of the minimum period
          //The H is always the pointer that point to the min
          //instance, and update the rest of the instance of the curr_ctr
          //if (H->nxt_match_ctr == APP_1SEC_CTR) {
          if (H->remain_ctr == 1) {


            if (t1_leftLed.remain_ctr ==1) {
                //leftLEDBlink_Exe_PRIO = 5;
                //OSTaskCreate((OS_TCB     *)&leftLEDBlinkTCB, (CPU_CHAR   *)"leftLEDBlink", (OS_TASK_PTR ) leftLEDBlink, (void       *) 0, (OS_PRIO     ) leftLEDBlink_Exe_PRIO, (CPU_STK    *)&leftLEDBlinkStk[0], (CPU_STK_SIZE) leftLEDBlink_STK_SIZE / 10u, (CPU_STK_SIZE) leftLEDBlink_STK_SIZE, (OS_MSG_QTY  ) 0u, (OS_TICK     ) 0u, (void       *) (CPU_INT32U) 2, (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), (OS_ERR     *)&err);
                //while(leftLedDone != 1){};  //keep looping until the task is done
                //leftLedDone = 0;
                leftLEDBlink_executed = 1;
                
                t1_leftLed.exe = 1;
                //Delete this node and Perform Sorting of the remaining nodes upon deletion of this node,
                Fibo_Sort(&t1_leftLed);    
                
                //Node deletion printf function call
                //#pragma __printf_args
                //printf ("\nnode deletion of period %d\n",t1_leftLed.n);
                
                leftLEDBlink_Exe_PRIO = 20;
                //H=Extract_Min(); //update the min in H pointer, already did in Fibo_sort
                
                // update for next release of the T1 task intance
                t1_leftLed.cur_ctr = APP_1SEC_CTR;//update for next occurrance
                t1_leftLed.nxt_match_ctr = t1_leftLed.n + t1_leftLed.cur_ctr; // n is period
                t1_leftLed.remain_ctr = t1_leftLed.nxt_match_ctr - t1_leftLed.cur_ctr; // cal remain count
                
                //insert back
                H = Insert(H,&t1_leftLed);
                
               //calculate deadline
                t1_deadline = t1_leftLed.cur_ctr + t1_leftLed.n;
                add_to_rdy_q_node(&t1_rdy,t1_deadline,t1_leftLed.n);
            }
            

            if (t2_rightLed.remain_ctr ==1) {
                //rightLEDBlink_Exe_PRIO = 7;
                //OSTaskCreate((OS_TCB     *)&rightLEDBlinkTCB, (CPU_CHAR   *)"rightLEDBlink", (OS_TASK_PTR ) rightLEDBlink, (void       *) 0, (OS_PRIO     ) rightLEDBlink_Exe_PRIO, (CPU_STK    *)&rightLEDBlinkStk[0], (CPU_STK_SIZE) rightLEDBlink_STK_SIZE / 10u, (CPU_STK_SIZE) rightLEDBlink_STK_SIZE, (OS_MSG_QTY  ) 0u, (OS_TICK     ) 0u, (void       *) (CPU_INT32U) 2, (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), (OS_ERR     *)&err);
                //while(rightLedDone != 1){};  //keep looping until the task is done
                //rightLedDone = 0;
                rightLEDBlink_executed = 1;
                t2_rightLed.exe = 1;
                //Delete this node and Perform Sorting upon deletion of this node
                Fibo_Sort(&t2_rightLed); 
                
                rightLEDBlink_Exe_PRIO = 20;
                //H=Extract_Min(); //update the min in H pointer, already did in Fibo_sort
                
                t2_rightLed.cur_ctr = APP_1SEC_CTR;//update for next occurrance
                t2_rightLed.nxt_match_ctr = t2_rightLed.n + t2_rightLed.cur_ctr; // n is period
                t2_rightLed.remain_ctr = t2_rightLed.nxt_match_ctr - t2_rightLed.cur_ctr; // cal remain count    
                
                //insert back
                H = Insert(H,&t2_rightLed); 
                
                //calculate deadline
                t2_deadline = t2_rightLed.cur_ctr + t2_rightLed.n;
                add_to_rdy_q_node(&t2_rdy,t2_deadline,t2_rightLed.n);               
                
            }
            

            if (t3_BothLed.remain_ctr ==1) {
              //BothLEDBlink_Exe_PRIO = 6;
              //OSTaskCreate((OS_TCB     *)&BothLEDBlinkTCB, (CPU_CHAR   *)"BothLEDBlink", (OS_TASK_PTR ) BothLEDBlink, (void       *) 0, (OS_PRIO     ) BothLEDBlink_Exe_PRIO, (CPU_STK    *)&BothLEDBlinkStk[0], (CPU_STK_SIZE) BothLEDBlink_STK_SIZE / 10u, (CPU_STK_SIZE) BothLEDBlink_STK_SIZE, (OS_MSG_QTY  ) 0u, (OS_TICK     ) 0u, (void       *) (CPU_INT32U) 2, (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), (OS_ERR     *)&err);
              //while(BothLedDone != 1){};  //keep looping until the task is done
              //BothLedDone = 0;
              BothLEDBlink_executed = 1;
              t3_BothLed.exe = 1;
                //Delete this node and Perform Sorting upon deletion of this node
                Fibo_Sort(&t3_BothLed);
                
                BothLEDBlink_Exe_PRIO = 20;
                //H=Extract_Min(); //update the min in H pointer, already did in Fibo_sort
                
                t3_BothLed.cur_ctr = APP_1SEC_CTR;//update for next occurrance
                t3_BothLed.nxt_match_ctr = t3_BothLed.n + t3_BothLed.cur_ctr; // n is period
                t3_BothLed.remain_ctr = t3_BothLed.nxt_match_ctr - t3_BothLed.cur_ctr; // cal remain count 

                //insert back
                H = Insert(H,&t3_BothLed);       
                
              //calculate deadline
              t3_deadline = t3_BothLed.cur_ctr + t3_BothLed.n;
              add_to_rdy_q_node(&t3_rdy,t3_deadline,t3_BothLed.n);                
                
            }
            
 
            if (t4_moveForward.remain_ctr ==1) {
              //moveForward_Exe_PRIO = 6;
              //iMove = 2;
              //OSTaskCreate((OS_TCB     *)&moveForwardTCB, (CPU_CHAR   *)"moveForward", (OS_TASK_PTR ) moveForward, (void       *) 0, (OS_PRIO     ) moveForward_Exe_PRIO, (CPU_STK    *)&moveForwardStk[0], (CPU_STK_SIZE) moveForward_STK_SIZE / 10u, (CPU_STK_SIZE) moveForward_STK_SIZE, (OS_MSG_QTY  ) 0u, (OS_TICK     ) 0u, (void       *) (CPU_INT32U) 2, (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), (OS_ERR     *)&err);
              //while(BothLedDone != 1){};  //keep looping until the task is done
              //BothLedDone = 0;
              moveForward_executed = 1;
              t4_moveForward.exe = 1;
                //Delete this node and Perform Sorting upon deletion of this node
                Fibo_Sort(&t4_moveForward);
                
                moveForward_Exe_PRIO = 20;
                //H=Extract_Min(); //update the min in H pointer, already did in Fibo_sort
                
                t4_moveForward.cur_ctr = APP_1SEC_CTR;//update for next occurrance
                t4_moveForward.nxt_match_ctr = t4_moveForward.n + t4_moveForward.cur_ctr; // n is period
                t4_moveForward.remain_ctr = t4_moveForward.nxt_match_ctr - t4_moveForward.cur_ctr; // cal remain count 

                //insert back
                H = Insert(H,&t4_moveForward);       
                
                //calculate deadline
                t4_deadline = t4_moveForward.cur_ctr + t4_moveForward.n;
                add_to_rdy_q_node(&t4_rdy,t4_deadline,t4_moveForward.n);
            }
            
           if (t5_moveBackward.remain_ctr ==1) {
              //moveBackward_Exe_PRIO = 6;
              //iMove = 2;
              //OSTaskCreate((OS_TCB     *)&moveBackwardTCB, (CPU_CHAR   *)"moveBackward", (OS_TASK_PTR ) moveBackward, (void       *) 0, (OS_PRIO     ) moveBackward_Exe_PRIO, (CPU_STK    *)&moveBackwardStk[0], (CPU_STK_SIZE) moveBackward_STK_SIZE / 10u, (CPU_STK_SIZE) moveBackward_STK_SIZE, (OS_MSG_QTY  ) 0u, (OS_TICK     ) 0u, (void       *) (CPU_INT32U) 2, (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), (OS_ERR     *)&err);            
              //while(BothLedDone != 1){};  //keep looping until the task is done
              //BothLedDone = 0;
              moveBackward_executed = 1;
              t5_moveBackward.exe = 1;
                //Delete this node and Perform Sorting upon deletion of this node
                Fibo_Sort(&t5_moveBackward);
                
                moveBackward_Exe_PRIO = 20;
                //H=Extract_Min(); //update the min in H pointer, already did in Fibo_sort
                
                t5_moveBackward.cur_ctr = APP_1SEC_CTR;//update for next occurrance
                t5_moveBackward.nxt_match_ctr = t5_moveBackward.n + t5_moveBackward.cur_ctr; // n is period
                t5_moveBackward.remain_ctr = t5_moveBackward.nxt_match_ctr - t5_moveBackward.cur_ctr; // cal remain count 

                //insert back
                H = Insert(H,&t5_moveBackward);      
                
                //calculate deadline
                t5_deadline = t5_moveBackward.cur_ctr + t5_moveBackward.n;
                add_to_rdy_q_node(&t5_rdy,t5_deadline,t5_moveBackward.n);
            }     
            
                  
            
            //Increment for those not executed task instance
            if (BothLEDBlink_executed == 0) {
                t3_BothLed.cur_ctr = APP_1SEC_CTR;
                t3_BothLed.remain_ctr = t3_BothLed.nxt_match_ctr - t3_BothLed.cur_ctr; // cal remain count             
            }

            if (leftLEDBlink_executed == 0) {
                t1_leftLed.cur_ctr = APP_1SEC_CTR;
                t1_leftLed.remain_ctr = t1_leftLed.nxt_match_ctr - t1_leftLed.cur_ctr; // cal remain count         
            }
            
            if (rightLEDBlink_executed == 0) {
                t2_rightLed.cur_ctr = APP_1SEC_CTR;
                t2_rightLed.remain_ctr = t2_rightLed.nxt_match_ctr - t2_rightLed.cur_ctr; // cal remain count            
            }   
            
            if (moveForward_executed == 0) {
                t4_moveForward.cur_ctr = APP_1SEC_CTR;
                t4_moveForward.remain_ctr = t4_moveForward.nxt_match_ctr - t4_moveForward.cur_ctr; // cal remain count            
            }   
            
            if (moveBackward_executed == 0) {
                t5_moveBackward.cur_ctr = APP_1SEC_CTR;
                t5_moveBackward.remain_ctr = t5_moveBackward.nxt_match_ctr - t5_moveBackward.cur_ctr; // cal remain count            
            }   
            
            BothLEDBlink_executed = 0;
            leftLEDBlink_executed = 0;
            rightLEDBlink_executed = 0;
            moveForward_executed = 0;
            moveBackward_executed = 0;
            /*
            t1_leftLed.exe = 0;
            t2_rightLed.exe = 0;
            t3_BothLed.exe = 0;
            t4_moveForward.exe = 0;
            t5_moveBackward.exe = 0;
            */
          } else 
            { //else, update the rest of the instance of the curr_ctr
              t1_leftLed.cur_ctr = APP_1SEC_CTR;
              t1_leftLed.remain_ctr = t1_leftLed.nxt_match_ctr - t1_leftLed.cur_ctr; // cal remain count
              
              t2_rightLed.cur_ctr = APP_1SEC_CTR;
              t2_rightLed.remain_ctr = t2_rightLed.nxt_match_ctr - t2_rightLed.cur_ctr; // cal remain count     
              
              t3_BothLed.cur_ctr = APP_1SEC_CTR;
              t3_BothLed.remain_ctr = t3_BothLed.nxt_match_ctr - t3_BothLed.cur_ctr; // cal remain count 
              
              
              t4_moveForward.cur_ctr = APP_1SEC_CTR;
              t4_moveForward.remain_ctr = t4_moveForward.nxt_match_ctr - t4_moveForward.cur_ctr; // cal remain count        
              
              t5_moveBackward.cur_ctr = APP_1SEC_CTR;
              t5_moveBackward.remain_ctr = t5_moveBackward.nxt_match_ctr - t5_moveBackward.cur_ctr; // cal remain count      
              
            }

/*********************************************************************************************************
          
          //Do all of the EDF scheduling here

*********************************************************************************************************/          
       
       prev_prior = 5;
       //if (t1_leftLed.exe == 1 | t2_rightLed.exe == 1 | t3_BothLed.exe == 1 | t4_moveForward.exe == 1 | t5_moveBackward.exe == 1){
       while (root != nil) {
         
         if (avl_exe_right == 0){min_rdy_q_node = minimum(root);}
         else {min_rdy_q_node = root->right;avl_exe_right =0;}

         min = min_rdy_q_node->value;
    
         //Always assign the highest priority to the KEY = DEADLINE TICK TIME - RELEASE TIME TIME
            
         if(min_rdy_q_node->n == 60) {
           moveBackward_Exe_PRIO = prev_prior;
           iMove = 2;
           OSTaskCreate((OS_TCB     *)&moveBackwardTCB, (CPU_CHAR   *)"moveBackward", (OS_TASK_PTR ) moveBackward, (void       *) 0, (OS_PRIO     ) moveBackward_Exe_PRIO, (CPU_STK    *)&moveBackwardStk[0], (CPU_STK_SIZE) moveBackward_STK_SIZE / 10u, (CPU_STK_SIZE) moveBackward_STK_SIZE, (OS_MSG_QTY  ) 0u, (OS_TICK     ) 0u, (void       *) (CPU_INT32U) 2, (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), (OS_ERR     *)&err);            
         }  

         if(min_rdy_q_node->n == 35) {
           moveForward_Exe_PRIO = prev_prior;
           iMove = 2;
           OSTaskCreate((OS_TCB     *)&moveForwardTCB, (CPU_CHAR   *)"moveForward", (OS_TASK_PTR ) moveForward, (void       *) 0, (OS_PRIO     ) moveForward_Exe_PRIO, (CPU_STK    *)&moveForwardStk[0], (CPU_STK_SIZE) moveForward_STK_SIZE / 10u, (CPU_STK_SIZE) moveForward_STK_SIZE, (OS_MSG_QTY  ) 0u, (OS_TICK     ) 0u, (void       *) (CPU_INT32U) 2, (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), (OS_ERR     *)&err);         
         }
         
         if(min_rdy_q_node->n == 20) {
           rightLEDBlink_Exe_PRIO = prev_prior;
           OSTaskCreate((OS_TCB     *)&rightLEDBlinkTCB, (CPU_CHAR   *)"rightLEDBlink", (OS_TASK_PTR ) rightLEDBlink, (void       *) 0, (OS_PRIO     ) rightLEDBlink_Exe_PRIO, (CPU_STK    *)&rightLEDBlinkStk[0], (CPU_STK_SIZE) rightLEDBlink_STK_SIZE / 10u, (CPU_STK_SIZE) rightLEDBlink_STK_SIZE, (OS_MSG_QTY  ) 0u, (OS_TICK     ) 0u, (void       *) (CPU_INT32U) 2, (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), (OS_ERR     *)&err);
  
         }
 
         if(min_rdy_q_node->n == 15) {
           BothLEDBlink_Exe_PRIO = prev_prior;
           OSTaskCreate((OS_TCB     *)&BothLEDBlinkTCB, (CPU_CHAR   *)"BothLEDBlink", (OS_TASK_PTR ) BothLEDBlink, (void       *) 0, (OS_PRIO     ) BothLEDBlink_Exe_PRIO, (CPU_STK    *)&BothLEDBlinkStk[0], (CPU_STK_SIZE) BothLEDBlink_STK_SIZE / 10u, (CPU_STK_SIZE) BothLEDBlink_STK_SIZE, (OS_MSG_QTY  ) 0u, (OS_TICK     ) 0u, (void       *) (CPU_INT32U) 2, (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), (OS_ERR     *)&err);
 
         }
         
         if(min_rdy_q_node->n == 5) {
           leftLEDBlink_Exe_PRIO = prev_prior;
           OSTaskCreate((OS_TCB     *)&leftLEDBlinkTCB, (CPU_CHAR   *)"leftLEDBlink", (OS_TASK_PTR ) leftLEDBlink, (void       *) 0, (OS_PRIO     ) leftLEDBlink_Exe_PRIO, (CPU_STK    *)&leftLEDBlinkStk[0], (CPU_STK_SIZE) leftLEDBlink_STK_SIZE / 10u, (CPU_STK_SIZE) leftLEDBlink_STK_SIZE, (OS_MSG_QTY  ) 0u, (OS_TICK     ) 0u, (void       *) (CPU_INT32U) 2, (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), (OS_ERR     *)&err);
         
         }

          //OverheadValue = StartTime2 +  OS_TS_GET() - StartTime2 - StartTime ;
            
         prev_prior = prev_prior + 1;
         if (tree_size == 2) {
           min_rdy_q_node = root->right;
           tree_size--;
           avl_exe_right = 1;
         }//if it is the last 2, there is a bug. do not delete first
         else if (tree_size == 1) {
           if (root->right != NULL) {
           root->right->parent = NULL;
           root->right->right = NULL;
           root->right->left = NULL;
           root->right->value = 0;
           root->right->n = 0;
           root->right->index = 0;}
           
           root->parent = NULL;
           root->right = NULL;
           root->left = NULL;
           root->value = 0;
           root->n = 0;
           root->index = 0;
           tree_size=0;
           root = NULL;
           h[0] = 0;h[1] = 0;h[2] = 0;h[3] = 0;h[4] = 0;h[5] = 0; 
         }//if it is the last node, re-init the root
         else { 
           delete_from_rdy_q_node(min);
           tree_size--;
         }
     
         
       }
            t1_leftLed.exe = 0;
            t2_rightLed.exe = 0;
            t3_BothLed.exe = 0;
            t4_moveForward.exe = 0;
            t5_moveBackward.exe = 0;      
       //}
          
             
    }
    
    //Do not delete this task
}

static void leftLEDBlink (void  *p_arg)
{
  /*
    OS_ERR      err;
    CPU_INT32U  i,k,j=0;
   
    for(i=0; i <(ONESECONDTICK); i++)
    {
      j = ((i * 2) + j);
    }
    
    BSP_LED_Off(2u);
    for(k=0; k<1; k++)
    {
      BSP_LED_Toggle(2u);
      for(i=0; i <ONESECONDTICK/2; i++)
         j = ((i * 2)+j);
    }
    
    BSP_LED_Off(2u);
    leftLedDone = 1;

   OSTaskDel((OS_TCB *)0, &err);
 */
     OS_ERR      err;
    CPU_INT32U  i,k,j=0;
   
  
    BSP_LED_On(2u);
    for(k=0; k<4; k++)
    {
      for(i=0; i <ONESECONDTICK; i++)
         j = ((i * 2)+j);
    }
	
    BSP_LED_Off(2u);
      leftLedDone = 1;
   OSTaskDel((OS_TCB *)0, &err); 
  
  
}  

static void rightLEDBlink (void  *p_arg)
{
/*
    OS_ERR      err;
    CPU_INT32U  i,k,j=0;
   
    for(i=0; i <(ONESECONDTICK); i++)
    {
      j = ((i * 2) + j);
    }
    
    BSP_LED_Off(1u);
    for(k=0; k<1; k++)
    {
      BSP_LED_Toggle(1u);
      for(i=0; i <ONESECONDTICK/2; i++)
         j = ((i * 2)+j);
    }
    
    BSP_LED_Off(1u);
    rightLedDone = 1;
    OSTaskDel((OS_TCB *)0, &err);
*/
    OS_ERR      err;
    CPU_INT32U  k, i, j;
    
    BSP_LED_On(1u);
    
    for(k=0; k<1; k++)
    {
      for(i=0; i <ONESECONDTICK; i++)
         j = ((i * 2)+j);
    }
    
    BSP_LED_Off(1u);
    rightLedDone = 1;
    OSTaskDel((OS_TCB *)0, &err);    
  
} 

static void BothLEDBlink (void  *p_arg)
{
  /*
    OS_ERR      err;
    CPU_INT32U  i,k,j=0;
   
    for(i=0; i <(ONESECONDTICK); i++)
    {
      j = ((i * 2) + j);
    }
    
    BSP_LED_Off(0u);
    for(k=0; k<1; k++)
    {
      BSP_LED_Toggle(0u);
      for(i=0; i <ONESECONDTICK/2; i++)
         j = ((i * 2)+j);
    }
    
    BSP_LED_Off(0u);
  */
    OS_ERR      err;
    CPU_INT32U  k, i, j;
    
    BSP_LED_On(0u);
    
    for(k=0; k<1; k++)
    {
      for(i=0; i <ONESECONDTICK; i++)
         j = ((i * 2)+j);
    }
    
    BSP_LED_Off(0u);
  
    BothLedDone = 1;
    OSTaskDel((OS_TCB *)0, &err);
} 

   static void  moveForward (void  *p_arg)
{ 
    OS_ERR      err;
    CPU_INT32U  k, i, j;
    
    if(iMove > 0)
    {
      if(iMove%2==0)
      {  
      //RoboTurn(FRONT, 16, 50);
      RoboTurn(FRONT, 4, 50);
      iMove--;
      }
      //else{
      //  RoboTurn(BACK, 16, 50);
      //  iMove++;
      //}
    }

    
    for(k=0; k<WORKLOAD1; k++)
    {
      for(i=0; i <ONESECONDTICK; i++){
        j=2*i;
      }
     }
   
    OSTaskDel((OS_TCB *)0, &err);   

}

  static void  moveBackward (void  *p_arg)
{ 
    OS_ERR      err;
    CPU_INT32U  k, i, j;
    
    if(iMove > 0)
    {
      if(iMove%2==0)
      {  
      RoboTurn(BACK, 4, 50);
      iMove--;
      }
     //else{
     //   RoboTurn(BACK, 16, 50);
      //  iMove++;
      //}
    }
    
    for(k=0; k<WORKLOAD1; k++)
    {
      for(i=0; i <ONESECONDTICK; i++){
        j=2*i;
      }
     }
    
    OSTaskDel((OS_TCB *)0, &err);   

}

static  void  AppRobotMotorDriveSensorEnable ()
{
    BSP_WheelSensorEnable();
    BSP_WheelSensorIntEnable(RIGHT_SIDE, SENSOR_A, (CPU_FNCT_VOID)IntWheelSensor);
    BSP_WheelSensorIntEnable(LEFT_SIDE, SENSOR_A, (CPU_FNCT_VOID)IntWheelSensor);
}


void IntWheelSensor()
{
	CPU_INT32U         ulStatusR_A;
	CPU_INT32U         ulStatusL_A;

	static CPU_INT08U CountL = 0;
	static CPU_INT08U CountR = 0;

	static CPU_INT08U data = 0;

	ulStatusR_A = GPIOPinIntStatus(RIGHT_IR_SENSOR_A_PORT, DEF_TRUE);
	ulStatusL_A = GPIOPinIntStatus(LEFT_IR_SENSOR_A_PORT, DEF_TRUE);

        if (ulStatusR_A & RIGHT_IR_SENSOR_A_PIN)
        {
          GPIOPinIntClear(RIGHT_IR_SENSOR_A_PORT, RIGHT_IR_SENSOR_A_PIN);           /* Clear interrupt.*/
          CountR = CountR + 1;
        }

        if (ulStatusL_A & LEFT_IR_SENSOR_A_PIN)
        {
          GPIOPinIntClear(LEFT_IR_SENSOR_A_PORT, LEFT_IR_SENSOR_A_PIN);
          CountL = CountL + 1;
        }

	if((CountL >= Left_tgt) && (CountR >= Right_tgt))
        {
          data = 0x11;
          Left_tgt = 0;
          Right_tgt = 0;
          CountL = 0;
          CountR = 0;
          BSP_MotorStop(LEFT_SIDE);
          BSP_MotorStop(RIGHT_SIDE);
        }
        else if(CountL >= Left_tgt)
        {
          data = 0x10;
          Left_tgt = 0;
          CountL = 0;
          BSP_MotorStop(LEFT_SIDE);
        }
        else if(CountR >= Right_tgt)
        {
          data = 0x01;
          Right_tgt = 0;
          CountR = 0;
          BSP_MotorStop(RIGHT_SIDE);
        }
        return;
}

void RoboTurn(tSide dir, CPU_INT16U seg, CPU_INT16U speed)
{
	Left_tgt = seg;
        Right_tgt = seg;

	BSP_MotorStop(LEFT_SIDE);
	BSP_MotorStop(RIGHT_SIDE);

        BSP_MotorSpeed(LEFT_SIDE, speed <<8u);
	BSP_MotorSpeed(RIGHT_SIDE,speed <<8u);

	switch(dir)
	{
            case FRONT :
                    BSP_MotorDir(RIGHT_SIDE,FORWARD);
                    BSP_MotorDir(LEFT_SIDE,FORWARD);
                    BSP_MotorRun(LEFT_SIDE);
                    BSP_MotorRun(RIGHT_SIDE);
                    break;
                    
            case BACK :
                    BSP_MotorDir(LEFT_SIDE,REVERSE);
                    BSP_MotorDir(RIGHT_SIDE,REVERSE);
                    BSP_MotorRun(RIGHT_SIDE);
                    BSP_MotorRun(LEFT_SIDE);
                    break;
                    
            case LEFT_SIDE :
                    BSP_MotorDir(RIGHT_SIDE,FORWARD);
                    BSP_MotorDir(LEFT_SIDE,REVERSE);
                    BSP_MotorRun(LEFT_SIDE);
                    BSP_MotorRun(RIGHT_SIDE);
                    break;
                    
            case RIGHT_SIDE:
                    BSP_MotorDir(LEFT_SIDE,FORWARD);
                    BSP_MotorDir(RIGHT_SIDE,REVERSE);
                    BSP_MotorRun(RIGHT_SIDE);
                    BSP_MotorRun(LEFT_SIDE);
                    break;
                    
            default:
                    BSP_MotorStop(LEFT_SIDE);
                    BSP_MotorStop(RIGHT_SIDE);
                    break;
	}

	return;
}

/*
*********************************************************************************************************
*                                                
*                                C++ Fucntion to Implement Fibonacci Heap
*
*********************************************************************************************************
*/

/*
 * Insert Node
 */

struct node* Insert(struct node* H, struct node* x)
{
    x->degree = 0;
    x->parent = NULL;
    x->child = NULL;
    x->left = NULL;
    x->right = NULL;

    if (H != NULL)
    {
        (H->left)->right = x;
        x->right = H;
        x->left = H->left;
        H->left = x;
        if (x->n < H->n)
            H = x;
    }
    else
    {
        H = x;
    }
    nH = nH + 1;
    return H;
}

/*
 * Extract Min Node in Fibonnaci Heap
 */
struct node* Extract_Min(struct node* x)
{
	
    //The min is always locate at the root list
    CPU_INT32U  min_fibo;
    struct node* p;
    struct node* x_tmp;
    struct node* H_tmp;
    struct node* x_org; //store  the orignal content
    /*
    //finding the minimum remain_ctr
    min = t1_leftLed.remain_ctr;
    p = &t1_leftLed;
    
    if (t2_rightLed.remain_ctr < min) {
          min = t2_rightLed.remain_ctr;
          p = &t2_rightLed;
    }

    if (t3_BothLed.remain_ctr < min) {
          min =t3_BothLed.remain_ctr;
          p = &t3_BothLed;
    }
    */
    x_org = x;
    min_fibo = x->remain_ctr;
    p = x;
    x_tmp = x; //make hard copy first
    H_tmp = x;//make hard copy first
    //Compare untill the left most node in the root list
    while (H_tmp != NULL) {
      if (H_tmp->left->remain_ctr < min_fibo){
        min_fibo = H_tmp->left->remain_ctr; 
        p = H_tmp->left;
      } else {
      H_tmp = H_tmp->left;
      }
    }

    //Compare untill the right most node in the root list
    while (x_tmp != NULL) {
      if (x_tmp->right->remain_ctr < min_fibo){
        min_fibo = x_tmp->right->remain_ctr; 
        p = x_tmp->right;
      } else {
      x_tmp = x_tmp->right;
      }
    }
    
    x = x_org;
    
    return p;
}

void Fibo_Sort(struct node* x)
{
  CPU_INT32U nH_tmp;
  CPU_INT32U degree_tmp;
  struct node* ptr_left_most;
  struct node* ptr_left_most_tmp;
  struct node* ptr_tmp;
  struct node* ptr_tmp2;
  struct node* ptr_tmp3;
  struct node* node_deg0;
  struct node* prev_node_deg0;
  struct node* node_deg_tmp;
  struct node* node_deg_tmp2;
  CPU_INT32U node_deg_in_between = 0;
  CPU_INT32U node_deg0_cnt = 0;
  CPU_INT32U num_root_node =0;
  CPU_INT32U result_main;
  CPU_INT32U result_right; 

  
  nH_tmp = nH;

  //First check the degree of each node, 
  //0 means no child, 
  //if 2 nodes have the same degree, perform merging
  //Always the pointer is the min,so it is always the parent node,no parent above
  //Loop until a NULL is reach
  //check that the delete node has no child
    if(x->degree == 0) {
      x->parent = NULL;
      x->child  = NULL;      
      //check and update the left of the deleted node
      if (x->left != NULL & x->right == NULL) {
        //copy the orignial deleted node to ptr_left_most;
        ptr_left_most =  (x->left);
        //perform update on the right of the left node to NULL
        (x->left)->right = NULL;    

      } else
      
      //check and update the right of the deleted node
      if (x->right != NULL & x->left == NULL) {
        //copy the orignial deleted node to ptr_left_most;
        ptr_left_most =  (x->right);
        //perform update on the left of the right node to NULL
        (x->right)->left = NULL;       
      } else
      
      //check and update the right of the deleted node
      if (x->right != NULL & x->left != NULL) {
        //copy the orignial deleted node to ptr_left_most;
        ptr_left_most =  (x->left);
        //perform connection of x->right to x->left
        (x->right)->left = (x->left);  
        (x->left)->right = (x->right);
        
      }
      x->right = NULL;
      x->left = NULL;    
    }
    else //if there is a child under the deleted node
    {
      //make all child node become root node
      //hard coded as thou as there is only 5 task at this moment
      //and there is no left and right node of the deleted task
      (x->child)->parent = NULL;
      (x->child->left)->parent = NULL;
      (x->child->right)->parent = NULL;
    
     
      //if the left and right of the child node is not a NULL
      if ((x->child)->left != NULL & (x->child)->right != NULL) {
              (x->child->left)->left = x->left;
              (x->child->right)->right = x->right;
              //Make connection of the root list to the child node
              (x->left)->right = (x->child)->left;
              (x->right)->left = (x->child)->right;
              //copy the orignial child of the  deleted node ;
              ptr_left_most =  x->child;
      } else//if the left of the child node is not a NULL but right is a NULL
        if ((x->child)->left != NULL & (x->child)->right == NULL) {
              (x->child->left)->left = x->left;
              (x->child)->right = x->right;
              //Make connection of the root list to the child node
              (x->left)->right = (x->child)->left;
              (x->right)->left = x->child;            
              //copy the orignial child of the  deleted node ;
              ptr_left_most =  x->child;
        } else//if the right of the child node is not a NULL but left is a NULL
          if ((x->child)->left == NULL & (x->child)->right != NULL) {
                (x->child)->left = x->left;   
                (x->child->right)->right = x->right;
                //Make connection of the root list to the child node
                (x->left)->right = x->child;
                (x->right)->left = (x->child)->right;  
                //copy the orignial child of the  deleted node ;
                ptr_left_most =  x->child;
          } else//if the left right of the child node is a NULL
            if ((x->child)->left == NULL & (x->child)->right == NULL) {
                (x->child)->left  = x->left;   
                (x->child)->right = x->right; 
                //Make connection of the root list to the child node
                (x->left)->right = x->child;
                (x->right)->left = x->child; 
                //copy the orignial child of the  deleted node ;
                ptr_left_most =  x->child;
            }
      
       //x->degree = x->degree  - 1;
      //Since the node is deleted from the list , the degree will be assign with 0
       x->degree = 0;
       x->right = NULL;
       x->left = NULL; 
       x->child = NULL; 
      
    }
    
    nH = nH - 1; //decrement 1 node
    nH_tmp = nH;
    
    //Must update the H -pointer to the latest min in the root list
      H = Extract_Min(ptr_left_most);
      
    //Perform Sorting after deletion, and added back the deleted node
    // Sorting base on remain_ctr
      
    //first rearrange all of the that is the same degree in 1 group
    //from smallest degree starting from left most and increasing to the right
    while (ptr_left_most->left != NULL) {
      ptr_left_most = ptr_left_most->left; 
    }      
    
    num_root_node = 0;
    ptr_left_most_tmp = ptr_left_most;
     while (ptr_left_most->right != NULL) {
      ptr_left_most = ptr_left_most->right;
      num_root_node = num_root_node + 1;//count number of node that is in the root list
    }      
    ptr_left_most = ptr_left_most_tmp;
    //First sort out all node that is degree 0, from left most till right at this moment first
    node_deg0_cnt = 0;
    node_deg_in_between = 0;
    num_root_node = num_root_node + 1; //+1 until the very last node
    prev_node_deg0 = NULL;
    prev_node_deg0->right = NULL;
    prev_node_deg0->left = NULL;
    while (num_root_node != 0) {
      
      if (ptr_left_most->degree == 0 & node_deg0_cnt == 0){
          prev_node_deg0 = ptr_left_most;
          node_deg0_cnt = 1;
      } else if (ptr_left_most->degree == 0 & node_deg0_cnt == 1 & ptr_left_most->left != prev_node_deg0 & ptr_left_most->left == prev_node_deg0->right & node_deg_in_between !=0) {
        //make a copy first of the middle node in between
        node_deg_tmp = ptr_left_most->left;
        node_deg_tmp2 = ptr_left_most->right;
        //making swap connection
        prev_node_deg0->right = ptr_left_most;
        ptr_left_most->left = prev_node_deg0;
        ptr_left_most->right = node_deg_tmp;
        node_deg_tmp->left = ptr_left_most;
        node_deg_tmp->right = node_deg_tmp2;
          
        node_deg0_cnt = 0; 
      } else {node_deg_in_between = ptr_left_most->degree;}
      
      ptr_left_most = ptr_left_most->right; 
      num_root_node = num_root_node - 1;
    } 
      
    
    //Finding the leftmost node untill a NULL is found from the left, starting from the the left
    //or right node from the previous deleted node
    //while (ptr_left_most->left != NULL) {
     // ptr_left_most = ptr_left_most->left; 
    //}
    ptr_left_most = ptr_left_most_tmp;
    //no sorting will be perform if not both of the remain_ctr is the same
    //Start Sorting starting from ptr_left_most, untill a NULL is found from the right of the ptr_left_most
    ptr_tmp2 = ptr_left_most;
    //while (ptr_tmp2->right != NULL) {
    while (ptr_tmp2 != NULL) {
        //if (ptr_left_most->degree ==  (ptr_left_most->right)->degree){
        if (ptr_tmp2->degree ==  (ptr_tmp2->right)->degree){
            //start a child note ,
            //store result to synchronize the number counter
          if (ptr_tmp2->exe == 1) {result_main = ptr_tmp2->remain_ctr + 1;} 
          else {result_main = ptr_tmp2->remain_ctr;}
          
          if ((ptr_tmp2->right)->exe == 1) {result_right = (ptr_tmp2->right)->remain_ctr + 1;}
          else {result_right = (ptr_tmp2->right)->remain_ctr;}
          
          //ptr_tmp = (ptr_tmp2->right)->right;//store the pointing at the right node of the ptr_tmp2->right
          
          //if (ptr_left_most->remain_ctr < (ptr_left_most->right)->remain_ctr) {
          if (result_main < result_right) {
             //make a full copy of the ptr_tmp2->right instance first
              ptr_tmp = (ptr_tmp2->right)->right;//store a copy of right node of the  ptr_tmp2 first
             
             //make ptr_tmp2->right a child node of ptr_tmp2
             //check if the child node is not a NULL first
              if (ptr_tmp2->child == NULL) {
                 ptr_tmp2->child = ptr_tmp2->right;
                (ptr_tmp2->right)->parent = ptr_tmp2;
                (ptr_tmp2->right)->left = NULL;
                (ptr_tmp2->right)->right = NULL;
              } else {
                //inserted as child 
                //and make connection to among the child node
                (ptr_tmp2->child)->left = ptr_tmp2->right;
                (ptr_tmp2->right)->right = ptr_tmp2->child;
                
                 ptr_tmp2->child = ptr_tmp2->right;
                (ptr_tmp2->right)->parent = ptr_tmp2;
                (ptr_tmp2->right)->left = NULL;
                                              
              }
             
             
            //connect the right of the child node to the ptr_tmp2
            //make ptr_tmp2 or left most as parent
            ptr_tmp2->right = ptr_tmp;
            ptr_tmp2->degree = ptr_tmp2->degree + 1;
            cur_degree =  ptr_tmp2->degree;
            
            if (ptr_tmp != NULL){ //assign only if the ptr_tmp is not a NULL
              ptr_tmp->left = ptr_tmp2;
            }
            
            
            //Check if the sorted degree is the same as from the previous one or not
            //If is the same, start from the leftmost position again
           // if (ptr_tmp2->degree == prev_degree) {
              if (cur_degree == prev_degree) {
                ptr_left_most = ptr_tmp2;
                while (ptr_left_most->left != NULL) {
                  ptr_left_most = ptr_left_most->left; 
                }
                ptr_tmp2 = ptr_left_most;//start from the beginining again                  
          } else { //starting from itself again
            /*
            if (ptr_tmp == NULL) {
              ptr_tmp2->right = NULL;
            } else {
              ptr_tmp2 = ptr_tmp;
            } */
            ptr_tmp2 = ptr_tmp2;
            //record the previous highest sorting degree
            prev_degree = cur_degree;
            }
          
            
          } 
          else if (result_main > result_right)
          { //ptr_left_most->right is less than ptr_left_most
            //To be done
             
            //make a full copy of the ptr_tmp2 instance first
             ptr_tmp = ptr_tmp2->right;//store the right node reference of ptr_tmp2 first
             
             if ((ptr_tmp2->right)->child == NULL) { //Check if the ptr_tmp2->right child node is a NULL or not
            //make ptr_tmp2 as child node
            (ptr_tmp2->right)->child = ptr_tmp2;         
            //connect to the left node of the ptr_tmp2
            (ptr_tmp2->right)->left = ptr_tmp2->left;//connect to the left node of the ptr_tmp2
            (ptr_tmp2->left)->right = ptr_tmp2->right; 
            
            (ptr_tmp2->right)->degree = (ptr_tmp2->right)->degree + 1;
            cur_degree =  (ptr_tmp2->right)->degree;
           
            //make ptr_tmp2 as child node
            ptr_tmp2->parent = ptr_tmp2->right;
            ptr_tmp2->left = NULL;
            ptr_tmp2->right = NULL;
             } else {
                // TO BE LED  FEWAFEWAFAWFAEWFWEFWEFWAFWEFWAFWEFWEFAWFWAEFFEW
                //inserted as child 
                //and make connection to among the child node
                //make a copy first, of all of the related notes
                ptr_tmp3 = (ptr_tmp2->right);
               
                (ptr_tmp2->right->child)->left = ptr_tmp2;            
                ptr_tmp2->right = ptr_tmp2->right->child;
                
                //cannot use ptr_tmp2->right anymore onward, must use all of the copies
                
                //make ptr_tmp2 as child node
                //(ptr_tmp2->right)->child = ptr_tmp2;   
                ptr_tmp3->child = ptr_tmp2; 
                //connect to the left node of the ptr_tmp2
                //(ptr_tmp2->right)->left = ptr_tmp2->left;//connect to the left node of the ptr_tmp2
                ptr_tmp3->left = ptr_tmp2->left;
                //(ptr_tmp2->left)->right = ptr_tmp2->right; 
                (ptr_tmp2->left)->right = ptr_tmp3;
            
                ptr_tmp3->degree = ptr_tmp3->degree + 1;
                cur_degree =  ptr_tmp3->degree;
           
                //make ptr_tmp2 as child node
                ptr_tmp2->parent = ptr_tmp3;
                ptr_tmp2->left = NULL;
                   
                              
             }
                    
            
            //Check if the sorted degree is the same as from the previous one or not
            //If is the same, start from the leftmost position again
            //if (ptr_tmp->degree == prev_degree) {
            if (cur_degree == prev_degree) {
                ptr_left_most = ptr_tmp;
                while (ptr_left_most->left != NULL) {
                  ptr_left_most = ptr_left_most->left; 
                }
                ptr_tmp2 = ptr_left_most;//start from the beginining again
            } else {//else resume the next right
            if (ptr_tmp != NULL) {  //assign only if it is not a NULL
                ptr_tmp2 = ptr_tmp;
            } else {
                //ptr_tmp2->right = NULL;
                ptr_tmp2 = NULL;
            }
            //record the previous highest sorting degree
            prev_degree = cur_degree;
            }
            
            
          } else //both are equal
          {
            //no action done here, only shift
            ptr_tmp2 = ptr_tmp2->right;
          }
              
     
        } else {
            
          ptr_tmp2 = ptr_tmp2->right;
        }
    }

   return;
}


/*
*********************************************************************************************************
*                                                
*                                C++ Fucntion to Implement AVL
*
*********************************************************************************************************
*/

struct rdy_q_node* init_tree() {
    //struct rdy_q_node avl_null_node;

	avl_null_node.left  = NULL;
        avl_null_node.right = NULL;
        avl_null_node.parent = NULL;
	avl_null_node.index = 0;
        tree_size = 0;
        return &avl_null_node;
	//nil = &x;
        //root = nil;
   
}

// does a single left rotation
void left_rotate(struct rdy_q_node * x) {
    struct rdy_q_node * y = x->right;
    x->right = y->left;
    if (y->left != nil) {
        y->left->parent = x;
    }
    y->parent = x->parent;
    if (x->parent == nil) {
        root = y;
    } else if (x == x->parent->left) {
        x->parent->left = y;
    } else {
        x->parent->right = y;
    }
    y->left = x;
    x->parent = y;
    
        return;
}

// does a single right rotation
void right_rotate(struct rdy_q_node * x) {
    struct rdy_q_node * y = x->left;
    x->left = y->right;
    if (y->right != nil) {
        y->right->parent = x;
    }
    y->parent = x->parent;
    if (x->parent == nil) {
        root = y;
    } else if (x == x->parent->right) {
        x->parent->right = y;
    } else {
        x->parent->left = y;
    }
    y->right = x;
    x->parent = y;
    
        return;
}

CPU_INT32U maximum(CPU_INT32U a, CPU_INT32U b) {

    
    if (a > b) return a;

    if (b > a) return b;
    

}

// returns the height of the subtree with root x
CPU_INT32U height(struct rdy_q_node * x) {
    if (x == nil) return 0;
    return maximum(height(x->left),height(x->right))+1;
}

// balances the tree after insert from leaves to the root
// currently you could observe that there is a problem in this code:
// we traverse the tree and begin balacing from every leaf when that's not actually neeeded
// instead we could keep a reference to the leaf node we have added and balance the tree upwards. The author leaves this to the reader
void balance(struct rdy_q_node * x) {
    if (x == nil) return;
    balance(x->left);
    balance(x->right);
    CPU_INT32U h1 = height(x->left);
    h[x->left->index] = h1;
    CPU_INT32U h2 = height(x->right);
    h[x->right->index] = h2;
    if (h1 - h2 == 2) {
        CPU_INT32U lh = h[x->left->left->index];
        CPU_INT32U rh = h[x->left->right->index];
        if (lh-rh == 1) {
            right_rotate(x);
        } else {
            // zig zag case
            left_rotate(x->left);
            right_rotate(x);
        }
    } else if (h1 - h2 == -2) {
        CPU_INT32U lh = h[x->right->left->index];
        CPU_INT32U rh = h[x->right->right->index];
        if (lh-rh == -1) {
            left_rotate(x);
        } else {
            // zig zag case
            right_rotate(x->right);
            left_rotate(x);
        }
    }
    
        return;
}

//void add_to_rdy_q_node(CPU_INT32U a, CPU_INT32U n) {
void add_to_rdy_q_node(struct rdy_q_node* tx_rdy,CPU_INT32U a, CPU_INT32U n) {
    struct rdy_q_node * root_tmp = root;
    struct rdy_q_node * y = nil;
    struct rdy_q_node new_node;
    struct rdy_q_node * z;

    tx_rdy->left = tx_rdy->right = tx_rdy->parent = nil;
    tx_rdy->index = ++tree_size;
    tx_rdy->n = n;
    tx_rdy->value = a;

    z = tx_rdy;
    while (root_tmp != nil) {
        y = root_tmp;
        if (z->value < root_tmp->value) {
            root_tmp = root_tmp->left;
        } else {
            root_tmp = root_tmp->right;
        }
    }
    z->parent = y;
    if (y == nil) {
        root = z;
    } else if(z->value < y->value) {
        y->left = z;
    } else {
        y->right = z;
    }
    balance(root);
    
        return;
}

// in-order print
void print(struct rdy_q_node * el, CPU_INT32U i) {
    if (el->left != nil) print(el->left,i);
    if (el->right != nil) print(el->right,i);
    
        return;
}

struct rdy_q_node* search(CPU_INT32U x) {
    //struct rdy_q_node * temp = new rdy_q_node[1];
    struct rdy_q_node * temp;
    temp = (struct rdy_q_node *) malloc(sizeof(struct rdy_q_node));
    
    temp = root;
    while (true) {
        if (temp == nil) {
            return nil;
        }
        if (temp->value == x) {
            return temp;
        }
        if (temp->value < x) {
            temp = temp->right;
            continue;
        }
        if(temp->value > x) {
            temp = temp->left;
            continue;
        }
    }
}

void transplant(struct rdy_q_node * node1,struct  rdy_q_node * node2) {
    if (node1->parent == nil) {
        root = node2;
    } else if (node1->parent->left == node1) {
        node1->parent->left = node2;
    } else {
        node1->parent->right = node2;
    }
    node2->parent = node1->parent;
    
        return;
}

struct rdy_q_node* minimum(struct rdy_q_node * node) {
    while (node->left != nil) node = node->left;
    return node;
}

void delete_balance(struct rdy_q_node * x) {
    CPU_INT32U h1 = height(x->left);
    h[x->left->index] = h1;
    CPU_INT32U h2 = height(x->right);
    h[x->right->index] = h2;

    if (h1-h2 == 2) {
        if (h[x->left->left->index]-h[x->left->right->index] == -1) {
            // zig zag cas
            left_rotate(x->left);
        }
        right_rotate(x);
    } else if (h1-h2 == -2) {
        if (h[x->right->left->index]-h[x->right->right->index] == 1) {
            // zig zag case
            right_rotate(x->right);
        }
        left_rotate(x);
    }
    if (x == root) return;
    delete_balance(x->parent);
    
        return;
}

void delete_from_rdy_q_node(CPU_INT32U val) {
   // struct rdy_q_node * x = new rdy_q_node[1];
   // struct rdy_q_node * y = new rdy_q_node[1];
   // struct rdy_q_node * q = new rdy_q_node[1];
    struct rdy_q_node * x;
    struct rdy_q_node * y;
    struct rdy_q_node * q;   
    x = (struct rdy_q_node *) malloc(sizeof(struct rdy_q_node));
    y = (struct rdy_q_node *) malloc(sizeof(struct rdy_q_node));
    q = (struct rdy_q_node *) malloc(sizeof(struct rdy_q_node));
    
    x = search(val);
    if (x == nil) {
        return;
    }
    q = x;
    if (x->left == nil) {
        transplant(x,x->right);
    } else if (x->right == nil) {
        transplant(x,x->left);
    } else {
        y = minimum(x->right);
        if (y->parent != x) {
            transplant(y,y->right);
            y->right = x->right;
            y->right->parent = y;
        }
        transplant(x,y);
        y->left = x->left;
        y->left->parent = y;
    }
    delete_balance(q);
    
        return;
}